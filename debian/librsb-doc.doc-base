Document: librsb-manual
Title: librsb Manual
Author: Michele Martone
Abstract: A sparse matrix library implementing the `Recursive Sparse
 Blocks' (RSB) matrix storage. This is the documentation for the
 application programming interface (API) of the `librsb' library. In
 order to use librsb, there is no need for the user to know the RSB
 layout and algorithms: this documentation should be sufficient. This
 library is dual-interfaced; it supports: a native (`RSB') interface
 (with identifiers prefixed by `rsb_' or `RSB_'), and a (mostly
 complete) Sparse BLAS interface, as a wrapper around the RSB
 interface. Many computationally intensive operations are implemented
 with thread parallelism, by using OpenMP. Thread parallelism can be
 turned off at configure time, if desired, or limited at execution
 time. Many of the computational kernels source code files (mostly
 internals) were automatically generated. This user documentation
 concerns the end user API only; that is, neither the internals, nor
 the code generator.
Section: Science/Mathematics

Format: HTML
Index: /usr/share/doc/librsb-dev/html/index.html
Files: /usr/share/doc/librsb-dev/html/*.html
