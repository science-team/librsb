librsb (1.3.0.2+dfsg-7) unstable; urgency=medium

  * Acknowledge NMUs. Thanks Steve and Benjamin!
  * d/control: Bump Standards-Version to 4.7.0 (no changes needed)
  * d/p/newline-latex-rsbtest.patch: New patch (Closes: 1088719)
  * d/librsb0t64.lintian-overrides: Drop unused override

 -- Rafael Laboissière <rafael@debian.org>  Sun, 01 Dec 2024 16:07:13 -0300

librsb (1.3.0.2+dfsg-6.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062613

 -- Benjamin Drung <bdrung@debian.org>  Wed, 28 Feb 2024 17:24:24 +0000

librsb (1.3.0.2+dfsg-6.1~exp1) experimental; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.

 -- Steve Langasek <vorlon@debian.org>  Fri, 02 Feb 2024 05:59:18 +0000

librsb (1.3.0.2+dfsg-6) unstable; urgency=medium

  * Upload to unstable

 -- Rafael Laboissière <rafael@debian.org>  Tue, 13 Jun 2023 06:19:25 -0300

librsb (1.3.0.2+dfsg-5) experimental; urgency=medium

  * d/rules:
    + Remove spurious -ffile-prefix-map option added by dpkg-buildflags
    + Avoid blhc false positives
    + Append -pipe to FCFLAGS in order to get around the build problem
      caused by using --parallel

 -- Rafael Laboissière <rafael@debian.org>  Mon, 06 Mar 2023 04:17:02 -0300

librsb (1.3.0.2+dfsg-4) unstable; urgency=medium

  * d/rules: Also specify no-parallel build on armel and i386

 -- Rafael Laboissière <rafael@debian.org>  Fri, 17 Feb 2023 17:52:27 -0300

librsb (1.3.0.2+dfsg-3) unstable; urgency=medium

  * d/rules: Specify no-parallel build on arm64.
    This should prevent FTBFS on this architecture.

 -- Rafael Laboissière <rafael@debian.org>  Fri, 17 Feb 2023 07:58:39 -0300

librsb (1.3.0.2+dfsg-2) unstable; urgency=medium

  * Drop coccinelle from Build-Depends list.
    Build-dependency on coccinelle was only necessary when compiling with
    gcc-11, which generated buggy code when option -O3 was used.
    Futhermore, to avoid inadvertently building on an out to date
    environment, build-dependency on gcc >= 12 is added and option
    --disable-extra-patches is now explicitly passed to the configure
    script. This change prevents FTBFS on amrhf, on which coccinelle has
    been removed. (Closes: #1030941)
  * d/control: Bump Standards-Version to 4.6.2 (no changes needed)

 -- Rafael Laboissière <rafael@debian.org>  Thu, 16 Feb 2023 09:33:44 -0300

librsb (1.3.0.2+dfsg-1) unstable; urgency=medium

  [ Rafael Laboissière ]
  * New upstream version 1.3.0.2+dfsg
  * d/p/no-utf8x-inputenc.patch: Drop patch (applied upstream)

  [ Debian Janitor ]
  * Apply multi-arch hints. + librsb0: Add Multi-Arch: same.

 -- Rafael Laboissière <rafael@debian.org>  Tue, 20 Dec 2022 19:04:20 -0300

librsb (1.3.0.1+dfsg-3) unstable; urgency=medium

  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * d/p/no-utf8x-inputenc.patch: New patch
  * d/librsb-dev.lintian-overrides: Adjust to the new syntax

 -- Rafael Laboissière <rafael@debian.org>  Tue, 06 Dec 2022 18:40:44 -0300

librsb (1.3.0.1+dfsg-2) unstable; urgency=medium

  * d/control: Use more informative package descriptions

 -- Rafael Laboissière <rafael@debian.org>  Fri, 06 May 2022 11:54:29 -0300

librsb (1.3.0.1+dfsg-1) unstable; urgency=medium

  * New upstream version 1.3.0.1+dfsg
  * Drop patches (applied upstream):
    + d/p/memory-aligned-access.patch:
    + d/p/drop-mtune-flag.patch
  * d/control: Set Multi-Arch foreign for librsb-doc
  * d/rules: Use CPFFLAGS, CFLAGS, and LDFLAS when compiling rsb127_bugfix
  * d/watch: Restraint upstream version number to digits and dots

 -- Rafael Laboissière <rafael@debian.org>  Sun, 01 May 2022 08:53:55 -0300

librsb (1.3.0.0+dfsg-6) unstable; urgency=medium

  * d/t/control: Depends on pkg-config
  * d/p/memory-aligned-access.patch: Update patch

 -- Rafael Laboissière <rafael@debian.org>  Fri, 28 Jan 2022 05:44:12 -0300

librsb (1.3.0.0+dfsg-5) unstable; urgency=medium

  * Fix autopkgtest failure
    + d/t/control:
      - Depends on g++
      - Add Restrictions: allow-stderr
    + d/p/prioritize-g++.patch: Drop obsolete patch
  * d/control: Drop useless dependency on ${shlib:Depends} for librsb-dev

 -- Rafael Laboissière <rafael@debian.org>  Wed, 26 Jan 2022 17:56:28 -0300

librsb (1.3.0.0+dfsg-4) unstable; urgency=medium

  * d/p/prioritize-g++.patch: New patch
  * d/p/drop-mtune-flag.patch: New patch

 -- Rafael Laboissière <rafael@debian.org>  Wed, 26 Jan 2022 03:18:21 -0300

librsb (1.3.0.0+dfsg-3) unstable; urgency=medium

  * d/p/memory-aligned-access.patch: New patch

 -- Rafael Laboissière <rafael@debian.org>  Tue, 25 Jan 2022 05:10:19 -0300

librsb (1.3.0.0+dfsg-2) unstable; urgency=medium

  * d/librsb-dev.examples: Install the sources of the matrix files (*.mtx)
  * Proper install of the pkgconfig file
    + debian/librsb-dev.install:
      - Do not use dh-exec
      - Make the file not executable
      - Move the installed directory usr/lib/*/pkgconfig/
    + debian/rules: Give option --enable-pkg-config-install to configure
    + d/control: Drop build-dependency on dh-exec

 -- Rafael Laboissière <rafael@debian.org>  Mon, 24 Jan 2022 09:06:12 -0300

librsb (1.3.0.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.3.0.0+dfsg
  * d/copyright:
    + Reflect upstream changes
    + Exclude directory rsblib/html
  * d/control: Build-depend on coccinelle
  * d/clean: Drop files cleaned upstream
  * d/librsb-dev.install: Install new manpages
  * d/librsb0.install: Install README.md
  * d/rules:
    + Add --enable-extra-patches option to configure
    + Add additional clean up
  * d/s/options: Drop obsolete entries in extend-diff-ignore

 -- Rafael Laboissière <rafael@debian.org>  Sun, 23 Jan 2022 14:16:29 -0300

librsb (1.2.0.10+dfsg-1) unstable; urgency=medium

  * New upstream version 1.2.0.10+dfsg
  * d/control: Bump Standards-Version to 4.6.0 (no changes needed)

 -- Rafael Laboissière <rafael@debian.org>  Fri, 17 Sep 2021 04:12:48 -0300

librsb (1.2.0.9+real+dfsg-1) unstable; urgency=medium

  * New upstream version 1.2.0.9+real+dfsg
    The previous version was released using a release-candidate
    tarball, hence the strange upstream version number (with "+real")
    for the current release.
  * d/p/typo-doxygen-alias.patch: Drop patch (applied upstream)

 -- Rafael Laboissière <rafael@debian.org>  Thu, 13 Aug 2020 03:12:42 -0300

librsb (1.2.0.9+dfsg-1) unstable; urgency=medium

  * New upstream version 1.2.0.9+dfsg
  * d/copyright: Reflect upstream changes
  * d/p/fix-path-of-mtx-files.patch: Drop patch (fixed upstream)
  * d/tests/compile-and-run-examples: New file.
    This script replaces the old compile-examples.  Besides compiling, it
    also runs the compiled executables.
  * d/clean: Update list of files to remove

 -- Rafael Laboissière <rafael@debian.org>  Sun, 09 Aug 2020 11:12:04 -0300

librsb (1.2.0.8+dfsg.1-2) unstable; urgency=medium

  * d/control:
    + Bump Standards-Version to 4.5.0 (no changes needed)
    + Bump debhelper compatibility level to 13
  * d/not-installed: Add list of exclusions for installation
  * Add new package librsb-tools (containing the rsbench program)
  * d/librsb-dev.install: Add man pages and examples
  * d/librsb0.install: Add AUTHORS and README
  * d/librsb-doc.lintian-overrides: Override duplicate-files
  * d/p/typo-doxygen-alias.patch: New patch
  * d/p/fix-path-of-mtx-files.patch: Add URL to Forwarded field
  * d/s/lintian-overrides: New file

 -- Rafael Laboissière <rafael@debian.org>  Sat, 01 Aug 2020 07:15:58 -0300

librsb (1.2.0.8+dfsg.1-1) unstable; urgency=medium

  * New upstream version 1.2.0.8+dfsg.1
  * d/librsb-doc.lintian-overrides: Drop unused override
  * d/copyright: Exclude the whole doc/html directory from the upstream tarball
  * d/s/lintian-overrides: Drop obsolete Lintian override
  * d/rules: Drop override_dh_compress
  * d/clean: Remove leftover file rsb-config.h.in~
  * d/control: Bump Standards-Version to 4.4.1 (no changes needed)

 -- Rafael Laboissière <rafael@debian.org>  Tue, 10 Dec 2019 02:26:57 -0300

librsb (1.2.0.8+dfsg-1) unstable; urgency=medium

  * New upstream version 1.2.0.8+dfsg
  * Adapt for new upstream tarball
    + d/copyright: Remove problematic JavaScript from the tarball
    + d/watch: Add "+dfsg" to upstream version number and repack the tarball
  * d/p/fix-spell-because.patch: Drop patch (applied upstream)
  * d/p/avoid-long-line-manpage.patch: Drop patch (applied upstream)
  * d/copyright: Reflect upstream changes

 -- Rafael Laboissiere <rafael@debian.org>  Thu, 18 Jul 2019 10:27:20 -0300

librsb (1.2.0.7-3) unstable; urgency=medium

  * d/t/control: Add lacking dependencies

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 14 Jul 2019 16:22:43 -0300

librsb (1.2.0.7-2) unstable; urgency=medium

  * d/t/control: Add dependencies on gcc and libc6-dev

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 14 Jul 2019 07:00:22 -0300

librsb (1.2.0.7-1) unstable; urgency=medium

  * New upstream version 1.2.0.7
    This is not really a new upstream version, but a change in the
    version numbering scheme, replacing the "-rc" by ".".  This
    avoids the Lintian warning about hyphen in the upstream part of
    the Debian changelog version.
  * d/watch:
    + Mangle upstream version (replace "-rc" by ".")
    + Mangle upstream URL for signature
  * d/control:
    + Bump Standards-Version to 4.4.0 (no changes needed)
    + Build-depend on help2man
  * Move rsbench program from librsb-dev into librsb0 package
  * d/p/avoid-long-line-manpage.patch: New patch
  * d/u/signing-key.asc: Add upstream signing key
  * Add unit tests for autopkgtest

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 12 Jul 2019 20:23:03 -0300

librsb (1.2.0-rc7-6) unstable; urgency=medium

  * d/control:
    + Add Rules-Requires-Root: no
    + Bump debhelper to compat level 12
    + Bump Standards-Version to 4.3.0 (no changes needed)
  * d/rules: Honor nocheck in DEB_BUILD_OPTIONS

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 06 Jan 2019 19:12:02 -0200

librsb (1.2.0-rc7-5) unstable; urgency=medium

  * Fix linking order (Closes: #875410). Thanks to Gianfranco Costamagna
    for the patch.

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 03 Jan 2018 21:09:44 -0200

librsb (1.2.0-rc7-4) unstable; urgency=medium

  * Bump debhelper compatibility level to 11
  * Drop use of autotools-dev
  * Adjust for HTML docs installed in package librsb-dev
  * d/rules: Fix errors in man pages
  * d/control:
    + Bump Standards-Version to 4.1.3 (no changes needed)
    + Use Debian's GitLab URLs in Vcs-* header
    + Build-depend on doxygen-latex
  * d/s/options: Extend list of ignored files in diff

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 29 Dec 2017 21:24:35 -0200

librsb (1.2.0-rc7-3) unstable; urgency=medium

  * d/p/fix-spell-because.patch: New patch
  * Add unit test for numerical bug fixed in version 1.2.0-rc7.
    Thanks to Michele Martone for the source file
  * d/control: Bump Standards-Version to 4.0.1 (no changes needed)

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 14 Aug 2017 05:13:12 -0300

librsb (1.2.0-rc7-2) unstable; urgency=medium

  * Upload to unstable
  * d/watch: Use version 4

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 18 Jun 2017 05:25:43 -0300

librsb (1.2.0-rc7-1) experimental; urgency=medium

  * New upstream version 1.2.0-rc7

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 05 Jun 2017 04:47:24 -0300

librsb (1.2.0-rc6-1) experimental; urgency=medium

  * New upstream version 1.2.0-rc6
  * Dropped patches (applied upstream):
    + d/p/use-shared-lib.patch
    + d/p/compile-fortran-examples.patch
    + d/p/fix-spelling-information.patch
    + d/p/fix-spelling-function.patch
    + d/p/manpages-in-section-1.patch
    + d/psenseful-whatis-entries.patch
  * d/copyright: Reflect upstream changes

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 25 Mar 2017 12:19:25 -0300

librsb (1.2.0-rc5-5) experimental; urgency=medium

  * d/control: Build-Depend on doxygen-latex
  * d/librsb-doc.lintian-overrides: New file

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 13 Mar 2017 04:41:04 -0300

librsb (1.2.0-rc5-4) experimental; urgency=medium

  [ Andreas Tille ]
  * Add citation

  [ Rafael Laboissiere ]
  * d/rules: Hardcode cache size on problematic architectures.
    Thanks to Michele Martone for the suggestion
  * Drop Lintian overrides for rc-version-greater-than-expected-version
  * d/control: Build-depends on help2man
  * New patches:
    + d/p/fix-spelling-information.patch
    + d/p/fix-spelling-function.patch
    + d/p/manpages-in-section-1.patch
    + d/p/senseful-whatis-entries.patch

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 11 Mar 2017 17:27:47 -0300

librsb (1.2.0-rc5-3) unstable; urgency=medium

  * d/control: Use secure URL in Vcs-Git
  * d/librsb-dev.examples: Exclude file benchex.sh
  * d/rules: Enable installation of Fortran modules
  * d/p/compile-fortran-examples.patch: New patch

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 09 Oct 2016 06:03:46 -0300

librsb (1.2.0-rc5-2) unstable; urgency=medium

  * Only add (uncompressed) source files as examples
  * debian/control: Fix URL in Vcs-Git
  * Bump debhelper compat level to 10
  * d/p/use-shared-lib.patch: New patch
  * d/p/fix-path-of-mtx-files.patch: New patch

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 08 Oct 2016 08:25:56 -0300

librsb (1.2.0-rc5-1) unstable; urgency=medium

  * Initial release (Closes: #838707)

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 23 Sep 2016 16:04:31 -0300
